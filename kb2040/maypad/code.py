# SPDX-FileCopyrightText: 2022 Eva Herrada for Adafruit Industries
# SPDX-License-Identifier: MIT

from kb import KMKKeyboard

from kmk.extensions.media_keys import MediaKeys
from kmk.keys import KC
from kmk.modules.layers import Layers
from kmk.handlers.sequences import simple_key_sequence

keyboard = KMKKeyboard()

media = MediaKeys()
layers_ext = Layers()

keyboard.extensions = [media]
keyboard.modules = [layers_ext]

# Cleaner key names
_______ = KC.TRNS
XXXXXXX = KC.NO

# sequences
TOGGLE_MIC_GOOGLE = simple_key_sequence((KC.LGUI(KC.D),))
TOGGLE_CAM_GOOGLE = simple_key_sequence((KC.LGUI(KC.E),))


keyboard.keymap = [
    [
        KC.A, KC.P7, KC.P8, KC.P9, KC.P9,
        KC.D, KC.P4, KC.P5, KC.P6, KC.P9,
        KC.G, KC.P1, KC.P2, KC.P3, KC.P9,
        TOGGLE_MIC_GOOGLE, TOGGLE_CAM_GOOGLE, KC.P0, KC.B, KC.P9,
    ]
]

if __name__ == '__main__':
    keyboard.go()