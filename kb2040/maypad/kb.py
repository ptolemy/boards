# SPDX-FileCopyrightText: 2022 Eva Herrada for Adafruit Industries
# SPDX-License-Identifier: MIT

import board

from kmk.kmk_keyboard import KMKKeyboard as _KMKKeyboard
from kmk.scanners import DiodeOrientation


class KMKKeyboard(_KMKKeyboard):
    row_pins = (board.A1, board.A0, board.SCK, board.MISO)
    col_pins = (board.D9, board.D8, board.D7, board.D6, board.D5)
    diode_orientation = DiodeOrientation.ROW2COL
    i2c = board.I2C